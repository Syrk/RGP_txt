﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG_txt.Character;


namespace RPG_txt.Stuff
{
  public class Item
  {
    
    public Item()
    {
    }
    //private String name;        // the name. every item got a name
    private String info;        // little infotext about the item

    private Health damage;      // the health of the item
    private String kind;        // what kind of sort is this item ("weapon"/"shield"...)
    //private double armor;       // to protect the person, it give extra armor
    private double magic;       // give person magic boost
    private double gold_value;  // the value to sell/buy the item
    //private double attack;      // the strength of the item. e.g. a weapon needs it to give damage

    


    //public String Name { get { return name; } set { name = value; } }
    public String Info { get { return info; } set { info = value; } }
    public Health Damage { get { return damage; } set { damage = value; } }
    public String Kind { get { return kind; } set { kind = value; } }
    //public double Armor { get { return armor; } set { armor = value; } }
    public double Magic { get { return magic; } set { magic = value; } }
    public double Gold_value { get { return gold_value; } set { gold_value = value; } }
    //public double Attack { get { return attack; } set { attack = value; } }

    public void repair()
    {
      damage.Cur_health = damage.Max_health;
    }
    

  }
}
