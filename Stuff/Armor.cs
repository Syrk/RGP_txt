﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RPG_txt.Character;

namespace RPG_txt.Stuff
{
  public class Armor : Item
  {
    public Armor():this(0.0,0.0)
    {
      
      base.Magic = 0.0;
    }
    public Armor(double param_damage):this(param_damage,0.0)
    {
    }
    public Armor(double param_damage, double param_magic)
    {
      base.Damage = new Health(param_damage);
      base.Magic = param_magic;
    }
  }
  public class Shield : Armor
  {
    public Shield(double param_damage):base(param_damage)
    {
      base.Kind = "Shield";
    }

  }
  public class Amulet : Armor
  {
    public Amulet(double param_damage):base(param_damage)
    {
      base.Kind = "Amulet";
    }

  }
  public class  Shoes: Armor
  {
    public Shoes(double param_damage):base(param_damage)
    {
      base.Kind = "Shoes";
    }

  }
  public class  Helmet: Armor
  {
    public Helmet(double param_damage):base(param_damage)
    {
      base.Kind = "Helmet";
    }
  }
  public class  Chest: Armor
  {
    public Chest(double param_damage):base(param_damage)
    {
      base.Kind = "Chest";
    }
  }
  public class  Belt: Armor
  {
    public Belt(double param_damage):base(param_damage)
    {
      base.Kind = "Belt";
    }
  }
  public class  Ring: Armor
  {
    public Ring(double param_damage):base(param_damage)
    {
      base.Kind = "Ring";
    }
  }
  public class  Leg_Protection: Armor
  {
    public Leg_Protection(double param_damage):base(param_damage)
    {
      base.Kind = "Leg_Protection";
    }
  }
  public class  Arm_Protection: Armor
  {
    public Arm_Protection(double param_damage):base(param_damage)
    {
      base.Kind = "Arm_Protection";
    }
  }
}
