﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPG_txt.Stuff
{
  /// <summary>
  /// This represents the current equipment of a person. Like his weapon he carry
  /// or his clothes he wear.
  /// </summary>
  public class Equipment
  {
    public Equipment()
    {

    }

    private Weapon           weapon_;
    private Shield           shield_;
    private Amulet           amulet_;
    private Shoes            shoes_ ;
    private Helmet           helmet_;
    private Chest            chest_ ;
    private Belt             belt_  ;
    private Ring             ring_;
    private Leg_Protection   leg_Protection_;
    private Arm_Protection   arm_Protection_;

    public Weapon Weapon_{ get { return weapon_; } set { weapon_ = value; } }
    public Shield             Shield_{ get { return shield_; } set { shield_ = value; } }
    public Amulet             Amulet_{ get { return amulet_; } set { amulet_ = value; } }
    public Shoes              Shoes_ { get { return shoes_; } set { shoes_ = value; } }
    public Helmet             Helmet_{ get { return helmet_; } set { helmet_ = value; } }
    public Chest              Chest_ { get { return chest_; } set { chest_ = value; } }
    public Belt               Belt_ { get { return belt_; } set { belt_ = value; } }
    public Ring               Ring_ { get { return ring_; } set { ring_ = value; } }
    public Leg_Protection     Leg_Protection_ { get { return leg_Protection_; } set { leg_Protection_ = value; } }
    public Arm_Protection     Arm_Protection_ { get { return arm_Protection_; } set { arm_Protection_ = value; } }
    
  }
}