﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_txt.Stuff
{
  public class Weapon : Item
  {
    public Weapon()
    {
      base.Kind = "Weapon";
    }
    private double attack;
    private string name;

    public double Attack { get { return attack; }set { attack = value; } }
    public string Name { get { return name; } set { name = value; } }
  }
}
