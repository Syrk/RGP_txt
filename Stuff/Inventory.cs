﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_txt.Stuff
{
  /// <summary>
  /// This class represents the bag. Every char can carry something in his bag.
  /// 
  /// This bag got an maximum amount of items.
  /// </summary>
  public class Inventory
  {
    public Inventory():this(0)
    {}
    public Inventory(double param_amount)
    {
      max_amount = param_amount;
      item_list = new List<Item>();
    }
    /// <summary>
    /// This method is to insert a item in the inventory
    /// </summary>
    /// <param name="param_insert"></param>
    /// <returns></returns>
    public int insert(Item param_insert)
    {
      if(item_list.Count < max_amount)
      {
        item_list.Add(param_insert);
        return 0; // everything is ok with insert
      }
      else
      {
        return -1;  // too much inventory
      }
    }
    public int inventory_amount()
    {
      return item_list.Count;
    }
    private List<Item> item_list;
    private double max_amount;
    public double Max_amount { get { return max_amount; }set { max_amount = value; } }

  }
}
