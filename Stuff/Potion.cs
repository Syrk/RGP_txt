﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_txt.Stuff
{
  class Potion : Item
  {
    public Potion()
    {
      base.Kind = "Potion";
      base.Magic = 0.0;
    }
    private double potion_value;
    public double Value { get { return potion_value; } set { potion_value = value; } }
  }

  class Health_Potion : Potion
  {
    public Health_Potion()
    {
      base.Info = "Ein Trank, der die Gesundheit bringt";// TODO here wrote some include for extern texts
      
    }
    
  }


  class Mana_Potion : Potion
  {
    public Mana_Potion()
    {
      base.Info = "Ein Trank, der Mana bringt.";  // TODO here too
    }
  }

}
