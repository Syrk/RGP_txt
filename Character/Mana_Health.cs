﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG_txt.Stuff;

namespace RPG_txt.Character
{
  /// <summary>
  /// This represents the health of the person.
  /// In max_health, you can see the maximum of health
  /// and in cur_health you see the current health of the person
  /// </summary>
  public class Health
  {
    public Health(double param_max)
    {
      max_health = param_max;
      cur_health = param_max;
    }


    private double max_health;
    private double cur_health;
    public double Max_health { get { return max_health; } set { max_health = value; } }
    public double Cur_health { get { return cur_health; } set { cur_health = value; } }

    /// <summary>
    /// This method will be used to give the person a potion
    /// </summary>
    /// <param name="potion"></param>
    internal void give_potion(Health_Potion potion)
    {
      if((Cur_health + potion.Value) >= Max_health)
      {
        Cur_health = Max_health;
      }
      else
      {
        Cur_health += potion.Value;
      }
    }
  }

  /// <summary>
  /// This represents the mana of the person.
  /// max_mana for maximum of mana and cur_mana for the current among of mana
  /// </summary>
  class Mana
  {
    public Mana(double param_max)
    {
      max_mana = param_max;
      cur_mana = param_max;
    }
    private double max_mana;
    private double cur_mana;
    public double Max_mana { get { return max_mana; } set { max_mana = value; } }
    public double Cur_mana { get { return cur_mana; } set { cur_mana = value; } }
    internal void give_potion(Mana_Potion potion)
    {
      if ((Cur_mana + potion.Value) >= Max_mana)
      {
        Cur_mana = Max_mana;
      }
      else
      {
        Cur_mana += potion.Value;
      }
    }
  }
  
}
