﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG_txt.Localization;
using RPG_txt.Stuff;

namespace RPG_txt.Character
{
  /// <summary>
  /// This class represents a person with his attributes and with his methods.
  /// 
  /// </summary>
  class Person
  {
    

    /// <summary>
    /// This are the attributes of the class "person".
    /// </summary>
    private String      name;
    private Boolean     male;
    private Int16       age;
    private Race        race;
    private Char_class  character_class;
    private Inventory   bag;
    private Equipment   equip;
    private Health      health;
    private Mana        mana;
    private Position    position;
    private Int32       gold;

    public String Name { get { return name; } set { name = value; } }
    public Boolean Male { get { return male; } set { male = value; } }
    public Int16 Age { get { return age; } set { age = value; } }
    public Race Race { get { return race; } set { race = value; } }
    public Char_class Character_class { get { return character_class; } set { character_class = value; } }
    public Inventory Bag { get { return bag; } set { bag = value; } }
    public Equipment Equip { get { return equip; } set { equip = value; } }
    public Health Health { get { return health; } set { health = value; } }
    public Mana Mana { get { return mana; } set { mana = value; } }
    public Position Position { get { return position; } set { position = value; } }
    public Int32 Gold { get { return gold; } set { gold = value; } }


    public Person()
    {
      
    }

    public void Use_Health_Potion(Health_Potion potion)
    {
      health.give_potion(potion);
    }

  }
}
