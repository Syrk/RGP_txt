﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_txt.Localization
{
  class Local_Map
  {
    public Local_Map()
    {
      map_positions = new List<Position.coord>();
    }

    private Position.coord global_Position;
    public Position.coord Global_Position { get { return global_Position; } set { global_Position = value; } }
    private List<Position.coord> map_positions;
  }
}
