﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_txt
{
  /// <summary>
  /// This class represents the position in the global map and in the local map.
  /// For example a person is in a village on the global map.
  /// But every map got his own local map. There can be something like a hospital
  /// or an item store etc etc. 
  /// </summary>
  public class Position
  {
    public struct coord
    {
      double x_coord;
      double y_coord;
    }

    private coord global_coord; // the coordinates of the world map. This could be the coordinate of a village
    
    private coord local_coord;  // the coordinates of the local map. This could be a hospital or a store on the local village map

    public coord Global_coord { get { return global_coord; } set { global_coord = value; } }
    public coord Local_coord { get { return local_coord; } set { local_coord = value; } }
  }
}